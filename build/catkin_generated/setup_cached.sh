#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export CATKIN_TEST_RESULTS_DIR="/home/nikita/ros/ros_example/build/test_results"
export ROS_TEST_RESULTS_DIR="/home/nikita/ros/ros_example/build/test_results"

# modified environment variables
export CMAKE_PREFIX_PATH="/home/nikita/ros/ros_example/build/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/nikita/ros/ros_example/build/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/nikita/ros/ros_example/build/devel/lib:/home/nikita/ros/ros_example/build/devel/lib/i386-linux-gnu:/opt/ros/indigo/lib/i386-linux-gnu:/opt/ros/indigo/lib"
export PATH="/home/nikita/ros/ros_example/build/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/nikita/ros/ros_example/build/devel/lib/pkgconfig:/home/nikita/ros/ros_example/build/devel/lib/i386-linux-gnu/pkgconfig:/opt/ros/indigo/lib/i386-linux-gnu/pkgconfig:/opt/ros/indigo/lib/pkgconfig"
export PYTHONPATH="/home/nikita/ros/ros_example/build/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/nikita/ros/ros_example/build/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/nikita/ros/ros_example:/opt/ros/indigo/share:/opt/ros/indigo/stacks"